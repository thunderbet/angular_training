import uuid from 'uuid/v4';

class Person {

    private id: string;
    protected name: string;

    constructor() {
        this.id = uuid.v4();
    }

    public getName(): string {
        return this.name;
    }

    public setName(name: string): void {
        this.name = name;
    }

    public getId(): string {
        return this.id;
    }

    public build(): Person {
        return new Person();
    }
}
