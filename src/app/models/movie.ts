/**
 * Class representing a movie
 */
export class Movie {

    id: number;
    title: string;
    synopsis: string;
    poster_path: string;
    backdrop_path: string;
    release_date: string;
    overview: string;
    genre_ids: number[];

    constructor(title: string, synopsis?: string, poster_path?: string, release_date?: string, overview? :string, genre_ids? :number[]) {
        this.title = title;
        this.synopsis = synopsis;
        this.poster_path = poster_path;
        this.release_date = release_date;
        this.overview = overview;
        this.genre_ids = genre_ids;
    }
}
