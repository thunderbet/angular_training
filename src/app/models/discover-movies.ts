/**
 * Classe representant la reponse du service discovery de TheMovieDB
 */
import {Movie} from './movie';

export class DiscoverMovies {
  page: number;
  total_results: number;
  total_pages: number;
  results: Movie[];
}
