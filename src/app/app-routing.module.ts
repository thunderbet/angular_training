import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MyMoviesComponent} from './movies/my-movies/my-movies.component';
import {DiscoverComponent} from './movies/discover/discover.component';


const routes: Routes = [
  { path: 'movies/my-movies', component: MyMoviesComponent },
  { path: 'movies/discover', component: DiscoverComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
