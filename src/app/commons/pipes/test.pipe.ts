import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'test'})
export class TestPipe implements PipeTransform {

  /**
   * Returns one out of 2 letters of any string
   */
  transform(value: any, ...args: any[]): any {
    let result = '';

    for (let i = 0; i < value.length; i++) {
      if (i % 2 === 0) {
        result += value[i];
      }
    }

    return result;
  }
}
