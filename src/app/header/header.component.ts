import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import ts, {SourceFile} from 'typescript';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    data: string;

    constructor(private httpClient: HttpClient) {
    }

    ngOnInit() {
        this.httpClient.get('assets/person.ts', {responseType: 'text'})
            .subscribe(content => this.test(content));
    }

    test(source) {
        let sourceFile: SourceFile;

        sourceFile = ts.createSourceFile(
            'x.ts',   // fileName
            source, // sourceText
            ts.ScriptTarget.Latest // langugeVersion
        );

        // @ts-ignore
        sourceFile.statements[1].forEachChild(child => {
                // @ts-ignore
            if (undefined != child.name) {
                    // @ts-ignore
                console.log(child.name.text);
                }
            }
        );

        // let members = sourceFile.statements[1].members;
        //
        // for (let i = 0; i < members.length; i++) {
        //   if (members[i].name !== undefined) {
        //     console.log(members[i].name.text);
        //   }
        // }
    }

}
