import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Movie} from '../models/movie';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MyMoviesService {

  private localUrl: string;

  constructor(private httpClient: HttpClient) {
    this.localUrl = 'http://localhost:3000/movies/';
  }

  /**
   * Methode qui retourne la liste des films
   */
  public getMovies(): Observable<Movie[]> {
    return this.httpClient.get<Movie[]>(this.localUrl);
  }
}
