import { TestBed } from '@angular/core/testing';

import { MyMoviesService } from './my-movies.service';
import {HttpClientModule} from '@angular/common/http';

describe('MyMoviesService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule]
  }));

  it('should be created', () => {
    const service: MyMoviesService = TestBed.get(MyMoviesService);
    expect(service).toBeTruthy();
  });
});
