import { TestBed } from '@angular/core/testing';

import { ThemoviedbService } from './themoviedb.service';
import {HttpClientModule} from '@angular/common/http';

describe('ThemoviedbService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule]
  }));

  it('should be created', () => {
    const service: ThemoviedbService = TestBed.get(ThemoviedbService);
    expect(service).toBeTruthy();
  });
});
