import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyMoviesComponent } from './movies/my-movies/my-movies.component';
import {MatButtonModule, MatInputModule, MatMenuModule} from '@angular/material';
import {FormsModule} from '@angular/forms';
import { MovieCardComponent } from './movies/movie-card/movie-card.component';
import {HttpClientModule} from '@angular/common/http';
import { TestPipe } from './commons/pipes/test.pipe';
import {DiscoverComponent} from './movies/discover/discover.component';
import {CardComponent} from './movies/card/card.component';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatGridListModule} from '@angular/material/grid-list';
import {FlexLayoutModule} from '@angular/flex-layout';
import {NgxYoutubePlayerModule} from 'ngx-youtube-player';
import {MatCardModule} from '@angular/material/card';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MyMoviesComponent,
    MovieCardComponent,
    DiscoverComponent,
    CardComponent,
    TestPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatMenuModule,
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    FormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatGridListModule,
    FlexLayoutModule,
    NgxYoutubePlayerModule
  ],
  exports: [],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

