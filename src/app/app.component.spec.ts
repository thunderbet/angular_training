import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {MatButtonModule} from '@angular/material/button';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCardModule} from '@angular/material/card';
import {FormsModule} from '@angular/forms';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatGridListModule} from '@angular/material/grid-list';
import {FlexLayoutModule} from '@angular/flex-layout';
import {NgxYoutubePlayerModule} from 'ngx-youtube-player';
import {MatMenuModule} from '@angular/material/menu';
import {MatInputModule} from '@angular/material/input';
import {MyMoviesComponent} from './movies/my-movies/my-movies.component';
import {DiscoverComponent} from './movies/discover/discover.component';
import {MovieCardComponent} from './movies/movie-card/movie-card.component';
import {CardComponent} from './movies/card/card.component';
import {TestPipe} from './commons/pipes/test.pipe';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        BrowserAnimationsModule,
        MatMenuModule,
        MatButtonModule,
        MatInputModule,
        MatCardModule,
        FormsModule,
        MatTableModule,
        MatPaginatorModule,
        MatGridListModule,
        FlexLayoutModule,
        NgxYoutubePlayerModule
      ],
      declarations: [
        AppComponent,
          HeaderComponent,
          FooterComponent,
          MyMoviesComponent,
          DiscoverComponent,
          MovieCardComponent,
          CardComponent,
          TestPipe
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'videotech'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('videotech');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('VideoTech');
  });
});
