import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Movie} from '../../models/movie';

@Component({
  selector: 'app-library-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  btnpartager = 'PARTAGER';
  color = '#4286f4';
  public imgPrefix = 'http://image.tmdb.org/t/p/w500/';

  @Input() movie: Movie;
  @Output() launchTrailer: EventEmitter<number> = new EventEmitter<number>();

  constructor() {  }

  ngOnInit() {
  }

  watchTrailer(movieId: number) {
    this.launchTrailer.emit(movieId);
  }

  /**
   * Retourne le style CSS pour l'image miniature
   */
  getMiniatureStyle(movie: Movie) {
    const styles = {
      'background-image': 'url(' + this.imgPrefix + movie.backdrop_path + ')',
      'background-size': 'cover'
    };

    return styles;
  }
}
