import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MyMoviesComponent} from './my-movies.component';
import {MatTableModule} from '@angular/material/table';
import {MatCardModule} from '@angular/material/card';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatGridListModule} from '@angular/material/grid-list';
import {HttpClientModule} from '@angular/common/http';
import {MyMoviesService} from '../../services/my-movies.service';
import {DiscoverComponent} from '../discover/discover.component';
import {Observable, of} from 'rxjs';
import {Movie} from '../../models/movie';
import {MovieCardComponent} from '../movie-card/movie-card.component';
import {FormsModule} from '@angular/forms';
import {TestPipe} from '../../commons/pipes/test.pipe';

class MockMyMovieService extends MyMoviesService {
    public getMovies(): Observable<Movie[]> {
        return of(Movie[0]);
    }
}

describe('MyMoviesComponent', () => {
    let component: MyMoviesComponent;
    let fixture: ComponentFixture<MyMoviesComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [MyMoviesComponent,
                MovieCardComponent,
                TestPipe],
            imports: [MatTableModule,
                MatCardModule,
                MatPaginatorModule,
                MatGridListModule,
                FormsModule,
                HttpClientModule],
            providers: [MyMoviesService]
        }).overrideComponent(
            DiscoverComponent,
            {set: {providers: [{provide: MyMoviesService, useClass: MockMyMovieService}]}}
        ).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MyMoviesComponent);
        component = fixture.componentInstance;
        component.movieName = 'Empty';
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
