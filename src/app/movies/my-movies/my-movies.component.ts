import { Component, OnInit } from '@angular/core';
import {MyMoviesService} from '../../services/my-movies.service';
import {Observable, of, Subject} from 'rxjs';
import {Movie} from '../../models/movie';
import {delay} from 'rxjs/operators';

@Component({
  selector: 'app-my-movies',
  templateUrl: './my-movies.component.html',
  styleUrls: ['./my-movies.component.scss']
})
export class MyMoviesComponent implements OnInit {

  public title: string;
  public movieName: string;
  public delayedMovie$: Subject<Movie> = new Subject<Movie>();
  public movies$: Subject<Movie[]> = new Subject<Movie[]>();

  constructor(private myMoviesService: MyMoviesService) { }

  ngOnInit() {
    this.title = 'My Movies';
    this.movieName = 'Jurassic Park';

    this.getAsyncData().subscribe(movie => {
      this.delayedMovie$.next(movie);
      console.log('Movie fetched');
    });

    this.getMovies();
  }

  outSourcedFunction() {
    alert('Well done!');
  }

  /**
   * Delays the response by 4 seconds
   */
  getAsyncData(): Observable<Movie> {
    // Fake Slow Async Data
    return of(
      new Movie('The Rise of Skywalker'))
      .pipe(delay(4000));
  }

  getMovies() {
    const movieObservables = this.myMoviesService.getMovies();

    movieObservables.subscribe(movies => {
      this.movies$.next(movies);
      console.log('Movie list fetched');
    });
  }
}
