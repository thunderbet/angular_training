import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DiscoverComponent} from './discover.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {NgxYoutubePlayerModule} from 'ngx-youtube-player';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {CardComponent} from '../card/card.component';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatCardModule} from '@angular/material/card';
import {ThemoviedbService} from '../../services/themoviedb.service';
import {Observable, of} from 'rxjs';
import {DiscoverMovies} from '../../models/discover-movies';
import {Movie} from '../../models/movie';
import {MovieVideos} from '../../models/movie-videos';
import {HttpClientModule} from '@angular/common/http';

class MockTheMovieDbService extends ThemoviedbService {
    public getDiscoverMovies(page: number): Observable<DiscoverMovies> {
        return of(new DiscoverMovies());
    }

    public getMovieVideos(movieId: number): Observable<MovieVideos> {
        return of(new MovieVideos());
    }
}

describe('DiscoverComponent', () => {
    let component: DiscoverComponent;
    let fixture: ComponentFixture<DiscoverComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [DiscoverComponent,
                CardComponent],
            imports: [MatTableModule,
                MatCardModule,
                MatPaginatorModule,
                MatGridListModule,
                HttpClientModule,
                FlexLayoutModule,
                NgxYoutubePlayerModule],
            providers: [ThemoviedbService]
        }).overrideComponent(
            DiscoverComponent,
            {set: {providers: [{provide: ThemoviedbService, useClass: MockTheMovieDbService}]}}
        )
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DiscoverComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
