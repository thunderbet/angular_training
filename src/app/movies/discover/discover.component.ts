import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject, Subscription} from 'rxjs';
import {Movie} from '../../models/movie';
import {ThemoviedbService} from '../../services/themoviedb.service';
import {MovieVideos} from '../../models/movie-videos';


@Component({
  selector: 'app-discover',
  templateUrl: './discover.component.html',
  styleUrls: ['./discover.component.scss']
})
export class DiscoverComponent implements OnInit, OnDestroy {
  private subscription: Subscription = new Subscription();
  private youtubePrefix = 'https://www.youtube.com/watch?v=';
  private imgPrefix = 'http://image.tmdb.org/t/p/w185/';

  trailerId = '';
  playerVars = {
    cc_lang_pref: 'en'
  };
  private player;
  private ytEvent;

  pageIndex = 1;
  totalMovies$: Subject<number> = new Subject<number>();
  movies$: Subject<Movie[]> = new Subject<Movie[]>();
  // Liste des colonnes qui s'affichent dans le tableau
  displayedColumns = ['id', 'title', 'releaseDate', 'posterPath', 'trailer'];

  constructor(private theMovieDbService: ThemoviedbService) { }

  ngOnInit() {
    this.getMovies();
  }

  onStateChange(event) {
    this.ytEvent = event.data;
  }

  savePlayer(player) {
    this.player = player;
  }

  playVideo() {
    this.player.playVideo();
  }

  pauseVideo() {
    this.player.pauseVideo();
  }

  /**
   * Recupere les films (1ere page)
   */
  getMovies() {
    const observableMovies = this.theMovieDbService.getDiscoverMovies(this.pageIndex);

    this.subscription.add(
      observableMovies.subscribe(data => {
        this.totalMovies$.next(data.total_results);
        this.movies$.next(data.results);
      })
    );
  }

  /**
   * Gere l'evenement de changement de page
   *
   */
  pageChangeEvent(event) {
    // Mise a jour de l'index de la page
    this.pageIndex = event.pageIndex;
    // Va chercher les nouveaux films
    this.getMovies();
  }

  showPoster(movie: Movie) {
    window.open(this.imgPrefix + movie.poster_path);
  }

  getImgUrl(movie: Movie): string {
    return this.imgPrefix + movie.poster_path;
  }

  /**
   * Lance la lecture d'un trailer
   *
   */
  watchTrailer(movieId: number) {
    const movieVideos = this.theMovieDbService.getMovieVideos(movieId);

    this.subscription.add(
      movieVideos.subscribe(data => {
        this.player.loadVideoById(this.getOfficialTrailer(data));
      })
    );
  }

  /**
   * Recupere la video de type trailer dans la liste de videos d'un film
   *
   */
  private getOfficialTrailer(movieVideos: MovieVideos): string {
    let officialTrailerUrl = '';

    const officalTrailerVideos = movieVideos.results.filter(video => {
      if (video.site === 'YouTube' && video.type === 'Trailer') {
        return true;
      }
    });

    if (officalTrailerVideos !== null) {
      officialTrailerUrl = officalTrailerVideos[0].key;
    }

    return officialTrailerUrl;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
